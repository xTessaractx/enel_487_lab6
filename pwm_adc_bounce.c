/**
Filename: serial_input_output.c
Author: Tessa Herzberger and Kiley LeClair
Class: enel487
Project: lab6
*/

#include "main.h"
#include "serial_driver_interface.h"

	static const float MAX_DC = 12;
	static const float MIN_DC = 3;

/**
		The pwm_adc_open function modifies the duty cycle
		to increase the height of the ball,
		thus to decrease the adc output value.
		It returns nothing.
*/
void pwm_adc_open (void)
{	
	float duty_cycle = 0;
	float dc_offset = 36;
	
	duty_cycle = dc_offset;
	duty_cycle = (((MAX_DC - MIN_DC) * (duty_cycle/100)) + MIN_DC);
	
	if (duty_cycle > MAX_DC)
		duty_cycle = MAX_DC;
	
	change_duty_pwm (duty_cycle);
}

/**
		The pwm_adc_close function modifies the duty cycle
		to decrease the height of the ball,
		thus to increase the adc output value.
		It returns nothing.
*/
void pwm_adc_close (void)
{
	float duty_cycle = 0;
	float dc_offset = 45;
	
	duty_cycle = *regTIM4_CCR2;
	duty_cycle = duty_cycle / 100;
	duty_cycle = ((duty_cycle - MIN_DC)/(MAX_DC - MIN_DC));
	duty_cycle = duty_cycle + dc_offset;
	duty_cycle = (((MAX_DC - MIN_DC) * (duty_cycle/100)) + MIN_DC);
	
	if (duty_cycle < MIN_DC)
		duty_cycle = MIN_DC;
	
	change_duty_pwm (duty_cycle);
}

/**
		The check_height function takes the adc output value
		that corresponds to the user input height,
		then determines whether or not the user input height
		is higher or lower than the actual height. It then calls
		the approproate function to either open or close the value.
		It returns nothing.
*/
void check_height (int wanted_adc_output)
{
	int adc_output = 0;
	float difference = 0;
	int i = 0;
	
	while (1)
	{		
		*regADC1_CR2 |= 0x00400000;
		adc_output = get_global_adc_output();
				
		difference = adc_output - wanted_adc_output;
		
		if (difference > 0)
		{
			pwm_adc_open();
		}
	
		else if (difference < 0)
		{
			pwm_adc_close();
		}
		
		for (i = 0; i < 240000; i++)
		{}
	}
}

/**
		The convert_cm_to_height function takes the user input height,
		then converts it to the corresponding adc output value.
		It then returns the adc output value.
*/
int convert_cm_to_height(float height)
{
	float inv_height = 0;
	float adc_output_float = 0;
	int adc_output = 0;
	
	inv_height = 1/height;
	
	adc_output_float = (29767 * inv_height) + 55.922;
	
	adc_output = (int) adc_output_float;
	
	return adc_output;
}
